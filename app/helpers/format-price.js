import {helper} from '@ember/component/helper';

export function formatPrice(price/*, hash*/) {
    return `$ ${price}`;
}

export default helper(formatPrice);
