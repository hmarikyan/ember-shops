import Component from '@ember/component';
import { observer } from '@ember/object';

export default Component.extend({
    products: null,
    sum: 0,

    didReceiveAttrs() {
        this._super(...arguments);

        this.calculatePrice();
    },

    obs: observer('products.@each.{price,quantity}', function() {
        this.calculatePrice();
    }),

    calculatePrice: function(){
        return this.get('products').then(products => {
            const sum = products.reduce((acc, p) => acc + parseInt(p.get('price')) * parseInt(p.get('quantity')), 0);
            this.set('totalPrice', sum);
        });
    }
});
