import Component from '@ember/component';

export default Component.extend({
    actions: {
        handleClose: function(){
            this.get('onClose')();
        }
    }
});
