import Component from '@ember/component';

export default Component.extend({
    actions: {
        onSubmit(e){
            e.preventDefault();
            this.get('onSubmit')();
        }
    }
});
