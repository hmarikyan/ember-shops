import Controller from '@ember/controller';

export default Controller.extend({
    dialogOpened: false,
    shopModel: null,
    links: null,

    init(){
        this._super(...arguments);

        //links used at the top of page
        this.set('links',[{title: 'Shops'}]);
    },

    resetForm(){
        this.set('shopModel', {
            name: '',
        });
    },

    actions: {

        //save p
        onSave(){
            const shopModel = this.get('shopModel');

            ///update existing shop record
            if(shopModel.save){
                shopModel.save();
            }else{
                //create new shop record
                const shop = this.store.createRecord('shop', shopModel);
                shop.save();
            }

            this.send('onDialogClose');
        },

        onDelete(shop){

            const products = shop.get('products');
            products.forEach(p => p.destroyRecord());

            shop.destroyRecord();
        },

        onDialogClose(){
            this.set('dialogOpened', false);

            this.resetForm();
        },

        onCreateDialogOpen(){
            this.resetForm();

            this.set('dialogOpened', true);
        },

        onEditDialogOpen(shop){
            this.set('shopModel', shop);

            this.set('dialogOpened', true);
        }
    }
});
