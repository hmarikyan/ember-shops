import Controller from '@ember/controller';

export default Controller.extend({
    dialogOpened: false,
    productModel: null,

    init(){
        this._super(...arguments);

        //links used at the top of page
        this.set('links',[
            {title: "shops", link: "shops"},
            {title: "products"}
        ]);
    },

    resetForm(){
        this.set('productModel', {
            name: '',
            quantity: 0,
            price: 0
        });
    },

    actions: {
        //create or update product
        onSave(){
            const shop = this.get('model');
            const productModel = this.get('productModel');

            ///update existing product
            if(productModel.save){
                productModel.save();
            }else{
                //create new product record
                const product = this.store.createRecord('product', productModel);

                shop.get('products').pushObject(product);

                product.save().then(() => {
                    shop.save();
                });
            }

            this.send('onDialogClose');
        },

        //delete shop
        onDelete(product){
            const shop = this.get('model');
            //remove relation
            shop.get('products').removeObject(product);
            shop.save();

            product.destroyRecord();
        },

        //close modal dialog
        onDialogClose(){
            this.set('dialogOpened', false);

            this.resetForm();
        },

        onCreateDialogOpen(){
            this.resetForm();

            this.set('dialogOpened', true);
        },

        onEditDialogOpen(product){
            this.set('productModel', product);

            this.set('dialogOpened', true);
        }
    }
});
