import Route from '@ember/routing/route';

export default Route.extend({
    model({shop_id}){
        return this.store.findRecord('shop', shop_id, {include: 'products'});
    }
});
