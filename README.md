# Shops

Experimental project which aims to get known with ember.js framework.

There are 2 pages in the project: shops and shop/products, 
in each page user can create/update/delete entities.

As a storage/backend server used firebase and 
 credentials are set to be able run the app without doing configrations.

## Installation

* `git clone <repository-url>` this repository
* `cd shops`
* `npm install`

## Running / Development

* `ember serve` or `npm start`
* Visit your app at [http://localhost:4200](http://localhost:4200).

### Running Tests

* `ember test`
* `ember test --server`

### Building

* `ember build` (development)
* `ember build --environment production` (production)
